/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

typedef struct
{
  const char *cmd PROGMEM;
  bool (*function)(Stream *);
} consoleCommand;

consoleCommand commands[] = {
  { "help", consoleHelp },
  { "exit", consoleExit },
  { "save", consoleSave },
  { "grant", programCard },
  { "initcard", initCard },
  { "secret", consoleSecret },
  { "resetCard", resetCard },
  { "dump", dumpCard }
};

bool consoleHelp(Stream *console)
{
  console->println(F("Known commands:\r\n"));

  for (int i = 0; i < sizeof(commands) / sizeof(commands[0]); i++)
  {
    console->println(commands[i].cmd);
  }
  return false;
}

bool consoleExit(Stream *console)
{
  return true;
}

bool consoleSave(Stream *console)
{
  eeprom_write_block((const void*)&settings, (void*)0, sizeof(settings));
  console->println(F("settings saved."));
  return true;
}

bool consoleSecret(Stream *console)
{
  console->println(F("WARNING: this will program a new master key and will invalidate all cards.\r\n"));
  console->println(F("Enter 12 hex digits (0-F). Leave empty to abort."));

  char lineBuf[64];
  console->print("> ");
  readLine(console, lineBuf, 64);

  if (strlen(lineBuf) == 12)
  {
    int pos  = 0;
    for (int i = 0; i < 6; i++)
    {
      unsigned char high_nibble, low_nibble, value;

      high_nibble = h2d(lineBuf[pos++]);
      low_nibble = h2d(lineBuf[pos++]);

      value = (high_nibble << 4) | low_nibble;

      settings.masterKey.keyByte[i] = value;
    }

    console->print(F("Key accepted. Do not forget to press save."));
  }

  return false;
}

void startConsole(Stream *console)
{
  console->println(F("Entering configuration mode. Enter help for commands.\r\n"));

  //eat away buffered input.
  while(Serial.available() > 0)
  {
    Serial.read();
  }

  char lineBuf[64];

  for (;;)
  {
    led(Blue);
    
    console->print("> ");
    if (readLine(console, lineBuf, 64))
    {
      console->println();
      led(Off);

      bool found = false;
      for (int i = 0; i < sizeof(commands) / sizeof(commands[0]); i++)
      {
        if (strcmp(lineBuf, commands[i].cmd) == 0)
        {
          found = true;
          if ((commands[i].function)(console))
          {
            return;
          }
        }
      }
      if (!found)
      {
        console->println(F("Unknown command"));
      }
    }
  }
}

/* echo everything from/to the ESP, until ^Z */
void terminal(Stream *console, Stream *esp)
{
  console->println(F("Starting terminal mode, enter ^Z to close."));

  for (;;)
  {
    while (console->available() > 0)
    {
      char c = console->read();
      if (c == 0x1A) //ctrl-z
      {
        console->println(F("Terminal closed."));
        return;
      }

      esp->write(c);
    }

    while (esp->available() > 0)
    {
      char c = esp->read();
      console->write(c);
    }
  }
}

bool readLine(Stream *stream, char *buf, size_t bufLen)
{
  int bufPos = 0;
  buf[bufPos] = '\0';

  for (;;)
  {
    if (stream->available() > 0)
    {
      char c = stream->read();
      //stream->print(c, HEX);
      stream->write(c);

      if (c == '\r')
      {
        continue;
      }

      if (c == '\b')
      {
        if (bufPos > 0)
        {
          //erase the character on the console and erase that one too.
          stream->write(' ');
          stream->write('\b');

          buf[--bufPos] = '\0';
          continue;
        }
      }

      if (c == '\n')
      {
        return true;
      }

      buf[bufPos++] = c;
      buf[bufPos] = '\0';

      //echo
      if (bufPos == bufLen)
      {
        return false;
      }
    }
  }
}

unsigned char h2d(unsigned char hex)
{
  if (hex > 0x39) hex -= 7; // adjust for hex letters upper or lower case
  return (hex & 0xf);
}

