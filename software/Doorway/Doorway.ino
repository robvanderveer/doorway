/* --------------------------------------
*             MFRC522      Arduino
*             Reader/PCD   Uno
* Signal      Pin          Pin
* --------------------------------------
* RST/Reset   RST          9
* SPI SS      SDA(SS)      10
* SPI MOSI    MOSI         11 / ICSP-4
* SPI MISO    MISO         12 / ICSP-1
* SPI SCK     SCK          13 / ICSP-3
*/


#include <SPI.h>
#include <MFRC522.h>
#include "timedEvent.h"

uint8_t const RST_PIN  =       9;
uint8_t const SS_PIN  =        10;
uint8_t const TONE_PIN =       6;
uint8_t const PIN_CONFIGURE =  A0;
uint8_t const PIN_LED_NO =    4;
uint8_t const PIN_LED_OK =    3;
uint8_t const PIN_LED_PRG =   5;
uint8_t const PIN_UNLOCK =    7;

byte const sector = 1;

typedef struct
{
  uint8_t masterCard[4];
  MFRC522::MIFARE_Key masterKey;
} Configuration;

typedef struct
{
  uint32_t accessBits;
  char firstName[12];
  uint16_t crc;
} cardPackage;


MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.
MFRC522::MIFARE_Key emptyKey;
Configuration settings;

typedef enum : int
{
  Off,
  Red,
  Green,
  Blue
} ledState;

void led(ledState rgb = Off)
{
  //  //these leds are pulldown.
  digitalWrite(PIN_LED_NO, (rgb == Red) ? LOW : HIGH);
  digitalWrite(PIN_LED_OK, (rgb == Green) ? LOW : HIGH);
  digitalWrite(PIN_LED_PRG, (rgb == Blue) ? LOW : HIGH);
}

void setup()
{
  pinMode(PIN_LED_PRG, OUTPUT);
  pinMode(PIN_LED_OK, OUTPUT);
  pinMode(PIN_LED_NO, OUTPUT);
  pinMode(PIN_UNLOCK, OUTPUT);
  pinMode(TONE_PIN, OUTPUT);
  led(Blue);
  digitalWrite(PIN_UNLOCK, LOW);

  Serial.begin(57600); // Initialize serial communications with the PC
  while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)

  Serial.println(F("Simplicate Doorway 1.0."));
  Serial.println();

  //verify the sizeof of the data package.
  if (sizeof(cardPackage) != 16 + 2)
  {
    Serial.print(F("FATAL ERROR UNEXPECTED cardPackage SIZE "));
    Serial.print(sizeof(cardPackage));
    Serial.println();
    beep(false);
    led(Red);
    while (1);
  }

  SPI.begin();        // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522 card
  ShowReaderDetails();  // Show details of PCD - MFRC522 Card Reader details
  mfrc522.PCD_SetAntennaGain(0b01110000); //set gain to max 48dB.

  //load the configuration from EEPROM
  eeprom_read_block((void*)&settings, (void *)0, sizeof(settings));

  // Prepare the key (used both as key A and as key B)
  // using FFFFFFFFFFFFh which is the default at chip delivery from the factory
  for (byte i = 0; i < 6; i++)
  {
    emptyKey.keyByte[i] = 0xFF;
  }

  for (byte i = 0; i < 6; i++)
  {
    Serial.println(settings.masterKey.keyByte[i], HEX);
  }

  Serial.println(F("Press 'S' to enter setup"));
  //allow 5 seconds.

  timedEvent timeout;
  while (!timeout.hasElapsed(5000) )
  {
    if (Serial.available() > 0)
    {
      char c = Serial.read();
      if (c == 'S')
      {
        startConsole(&Serial);
        break;
      }
    }
  }

  tone(TONE_PIN, 1760, 20);
  led(Red);
  delay(500);
  led(Green);
  delay(500);
  led(Blue);
  delay(500);
  led(Off);
}

void loop()
{
  // put your main code here, to run repeatedly:

  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent())
    return;

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial())
  {
    Serial.println(F("read error"));
    return;
  }

  if (isMatch())
  {
    Serial.println(F("Welcome."));

    digitalWrite(PIN_UNLOCK, HIGH);
    led(Green);
    beep(true);

    delay(500);

    digitalWrite(PIN_UNLOCK, LOW);
    led(Off);

  }
  else
  {
    Serial.println(F("Access denied."));

    beep(false);

    mfrc522.PICC_DumpToSerial(&(mfrc522.uid));


  }
}

void beep(bool ok)
{
  if (ok)
  {
    tone(TONE_PIN, 3520, 100);
  }
  else
  {
    for (int j = 0; j < 3; j++)
    {
      tone(TONE_PIN, 220, 50);
      led(Red);
      delay(100);
      led(Off);
      delay(100);
    }
  }
}

bool unlockWith(char block, MFRC522::MIFARE_Key *key)
{
  byte status;

  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK)
  {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));

    mfrc522.PCD_StopCrypto1();
    return false;
  }

  return true;
}

bool isMatch()
{
  byte trailerBlock   = sector * 4 + 3;
  byte block = sector * 4;
  byte status;

  if (unlockWith(block, &settings.masterKey) == false)
  {
    return false;
  }

  cardPackage package;

  byte size = 18;
  status = mfrc522.MIFARE_Read(block, (uint8_t *)&package, &size);
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();

  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Read() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));

    return false;
  }

  if (!(package.accessBits & 0x1))
  {
    return false;
  }

  dumpMask(&Serial, package.accessBits);

  Serial.print(F("Hello, "));
  Serial.println(package.firstName);

  return true;
}

void dumpMask(Stream *console, uint32_t mask)
{
  for (uint32_t b = 0; b < 32; b++)
  {
    Serial.print(((mask >> b) & 1) ? '1' : '0');
  }
  Serial.println();
}

bool programCard(Stream *console)
{
  //first we need to unlock the card.
  byte trailerBlock   = sector * 4 + 3;
  byte block = sector * 4;
  byte status;

  Serial.println(F("Enter name (max 12 chars): "));
  char buf[20];
  readLine(console, buf, 12);

  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();

  Serial.println(F("Present card to program"));

  if (!waitForCard(console))
  {
    beep(false);
    return false;
  }

  if (unlockWith(trailerBlock, &settings.masterKey) == false)
  {
    Serial.println(F("Card not initialized."));
    beep(false);
    return false;
  }

  Serial.println(F("Writing permissions."));

  //card unlocked, write sector.
  cardPackage package;
  package.accessBits = 0xFFFFAA01UL;

  dumpMask(console, package.accessBits);

  strncpy(package.firstName, buf, 11);

  //before writing, unlock the block with key A

  status = mfrc522.MIFARE_Write(block, (uint8_t *)&package, 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Write() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    beep(false);
    return false;
  }

  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();

  Serial.println(F("Permissions saved."));
  beep(true);
  return false;
}

bool initCard(Stream *console)
{
  //first we need to unlock the card.
  byte trailerBlock   = sector * 4 + 3;
  byte block = sector * 4;
  byte status;

  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();

  console->println(F("Present NEW card"));

  if (!waitForCard(console))
  {
    beep(false);
    return false;
  }

  console->println(F("Got a card."));

  if (unlockWith(trailerBlock, &emptyKey))
  {

    //we were able to unlock the card with the empty key.
    // To use the key, reprogram the trailer block with the masterKey on KEY A and set access bits.
    byte trailerBuffer[] = {
      255, 255, 255, 255, 255, 255,       // Keep default key A
      0, 0, 0,
      0,
      255, 255, 255, 255, 255, 255        // Keep default key B
    };

    console->println(F("Writing new sector trailer and access key..."));

    uint8_t dataAccessBits = 0b000;     //you just need A or B to read/write, good enough for now.
    uint8_t trailerAccessBits = 0b001;  //transport configuration, you just need key A to read/write, key B is readable data!!!

    //copy the master read key to KEY A
    for (int i = 0; i < 6; i++)
    {
      trailerBuffer[i] = settings.masterKey.keyByte[i];
    }

    mfrc522.MIFARE_SetAccessBits(&trailerBuffer[6], dataAccessBits, dataAccessBits, dataAccessBits, trailerAccessBits);        //all access bits to transport configuration
    status = mfrc522.MIFARE_Write(trailerBlock, trailerBuffer, 16);
    if (status != MFRC522::STATUS_OK) {
      console->print(F("MIFARE_Write() failed: "));
      console->println(mfrc522.GetStatusCodeName(status));
      beep(false);

      return false;
    }

    console->println(F("Card prepared."));

    mfrc522.PICC_HaltA();
    mfrc522.PCD_StopCrypto1();
    beep(true);

    return false;
  }
  else
  {
    console->println(F("Default unlock failed. This card may already be initialized."));
  }

  console->println(F("Init failed."));
  beep(false);

  return false;
}

bool dumpCard(Stream *console)
{
  if (!waitForCard(console))
  {
    return false;
  }


  mfrc522.PICC_DumpToSerial(&(mfrc522.uid));
  return false;
}

//remove the encryption
bool resetCard(Stream *console)
{
  //first we need to unlock the card.
  byte trailerBlock   = sector * 4 + 3;
  byte block = sector * 4;
  byte status;

  console->println(F("Present card to reset."));

  if (!waitForCard(console))
  {
    return false;
  }

  if (unlockWith(trailerBlock, &settings.masterKey) == false)
  {
    console->println(F("This card was not locked with the current key."));
    mfrc522.PCD_StopCrypto1();
    return false;
  }

  //we were able to unlock the card with the master key.
  // To use the key, reprogram the trailer block with the masterKey on KEY A and set access bits.
  byte trailerBuffer[] = {
    255, 255, 255, 255, 255, 255,       // Keep default key A
    0, 0, 0,
    0x69,                               // User data
    255, 255, 255, 255, 255, 255        // Keep default key B
  };

  console->println(F("Writing new sector trailer and access key..."));

  uint8_t dataAccessBits = 0b000;     //you just need A or B to read/write, good enough for now.
  uint8_t trailerAccessBits = 0b001;  //transport configuration, you just need key A to read/write, key B is readable data!!!

  mfrc522.MIFARE_SetAccessBits(&trailerBuffer[6], dataAccessBits, dataAccessBits, dataAccessBits, trailerAccessBits);        //all access bits to transport configuration
  status = mfrc522.MIFARE_Write(trailerBlock, trailerBuffer, 16);
  if (status != MFRC522::STATUS_OK)
  {
    console->print(F("MIFARE_Write() failed: "));
    console->println(mfrc522.GetStatusCodeName(status));
    return false;
  }

  cardPackage package;
  package.accessBits = 0x00000000;

  dumpMask(console, package.accessBits);

  strncpy_P(package.firstName, PSTR("__________"), 11);
  memset(package.firstName, 0, 12);

  status = mfrc522.MIFARE_Write(block, (uint8_t *)&package, 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Write() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    beep(false);
    return false;
  }

  console->println(F("Card cleared."));

  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  beep(true);
  return false;
}

bool waitForCard(Stream *console)
{
  unsigned long timestamp = millis();

  console->println(F("Waiting for card.."));
  while (millis() - timestamp < 5000)
  {
    if (! mfrc522.PICC_IsNewCardPresent())
    {
      continue;
    }

    if ( ! mfrc522.PICC_ReadCardSerial())
    {
      continue;
    }

    console->println(F("New card."));

    if (mfrc522.uid.size != 4)
    {
      console->println(F("Invalid UID size."));
      console->println(mfrc522.uid.size);

      mfrc522.PICC_DumpToSerial(&(mfrc522.uid));
      return false;
    }

    byte piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    console->println(mfrc522.PICC_GetTypeName(piccType));

    if (    piccType != MFRC522::PICC_TYPE_MIFARE_MINI
            &&  piccType != MFRC522::PICC_TYPE_MIFARE_1K
            &&  piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
      console->println(F("This sample only works with MIFARE Classic cards."));
      return false;
    }

    return true;
  }

  //timeout.
  return false;
}


void ShowReaderDetails() {
  // Get the MFRC522 software version
  byte v = mfrc522.PCD_ReadRegister(mfrc522.VersionReg);
  Serial.print(F("MFRC522 Software Version: 0x"));
  Serial.print(v, HEX);
  if (v == 0x91)
    Serial.print(F(" = v1.0"));
  else if (v == 0x92)
    Serial.print(F(" = v2.0"));
  else
    Serial.print(F(" (unknown)"));
  Serial.println("");
  // When 0x00 or 0xFF is returned, communication probably failed
  if ((v == 0x00) || (v == 0xFF)) {
    Serial.println(F("WARNING: Communication failure, is the MFRC522 properly connected?"));
  }
}

